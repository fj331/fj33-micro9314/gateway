package com.github.fwfurtado.micro9314.gateway.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.UUID;

@RestController
public class CompositionController {

    @GetMapping("composition")
    Map<String , UUID> composite() {
        return Map.of("id", UUID.randomUUID());
    }

}
