

build/package:
	./mvnw clean package

build/docker:
	docker image build -t micro9314/gateway:$VERSION .

build/docker/push:
	kind load docker-image --name kind-local

build:
	make build/package
	VERSION:=$(shell sha1sum target/gateway-0.0.1-SNAPSHOT.jar | cut -d ' ' -f 1 | cut -c 1-8)
	IMAGE_NAME:=micro9314/gateway:$VERSION
	make build/docker name=$IMAGE_NAME
	make build/docker/push name=$IMAGE_NAME

deploy/k8s:
	helm
