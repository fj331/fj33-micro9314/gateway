FROM openjdk:14-jdk-slim as builder

WORKDIR /build

COPY pom.xml /build
COPY .mvn/   /build/.mvn/
COPY mvnw   /build

RUN ./mvnw  dependency:resolve

COPY src/main   /build/src/main

RUN ./mvnw clean package -DskipTests

FROM openjdk:14-jdk-slim

ARG JAR

WORKDIR /app

COPY --from=builder /build/target/${JAR} /app/app.jar

CMD ["java", "--enable-preview", "-jar", "app.jar"]